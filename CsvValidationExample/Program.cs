﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

namespace CsvValidationExample
{
    class Program
    {
        public static FileHelperEngine fileHelperEngine;

        static void Main(string[] args)
        {
            ExModel [] models = ParseAndValidateCsv(@"..\..\CsvExample\example1.csv");
            Console.ReadLine();
        }

        public static ExModel[] ParseAndValidateCsv(string filePath)
        {
            ExModel[] models = null;
            
            fileHelperEngine = new FileHelperEngine(typeof(ExModel));
            fileHelperEngine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue; 
            models = fileHelperEngine.ReadFile(filePath) as ExModel[];
            if (fileHelperEngine.ErrorManager.HasErrors)
            {
                foreach (ErrorInfo err in fileHelperEngine.ErrorManager.Errors)
                {
                    Console.WriteLine(err.LineNumber);
                    Console.WriteLine(err.RecordString);
                    Console.WriteLine(err.ExceptionInfo.ToString());
                }
            }
            return models;
           
        }
    }
}
